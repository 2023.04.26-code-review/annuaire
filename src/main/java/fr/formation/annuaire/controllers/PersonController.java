package fr.formation.annuaire.controllers;

import java.util.Collection;

import fr.formation.annuaire.models.Person;
import fr.formation.annuaire.repositories.PersonRepository;

public class PersonController {

	private PersonRepository personRepository = new PersonRepository();
	
	public void findAll() {
		Collection<Person> people = personRepository.findAll();
		for (Person person : people) {
			System.out.println("\t" + person);
		}
	}
	
}
