package fr.formation.annuaire;

import java.util.Scanner;

import fr.formation.annuaire.controllers.PersonController;

public class App {
	
	public static void printChoices() {
		System.out.println("Actions :");
		System.out.println("1. find all");
		System.out.println("0. Quitter");
	}
	
	public static void main(String[] args) {
		try (Scanner scanner = new Scanner(System.in)) {
			PersonController personController = new PersonController();
			int choix;
			do {
				printChoices();
				choix = Integer.parseInt(scanner.nextLine());
				switch (choix) {
				case 1:
					personController.findAll();
					break;
				case 0:
					System.out.println("bye !");
					break;
				default:
					System.out.println("invalid choice");
					break;
				}
				
			} while (choix != 0);
		}
	}
}
