package fr.formation.annuaire.repositories;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import fr.formation.annuaire.models.Person;

public class PersonRepository {

	private Collection<Person> people = new ArrayList<>(Arrays.asList(new Person[] {
			new Person("andre", "123"),
			new Person("mohamed", "456"),
			new Person("margaux", "789")
	}));
	
	
	public Collection<Person> findAll() {
		return people;
	}
	
}
