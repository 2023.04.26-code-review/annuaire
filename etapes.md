# Étapes à suivre

## Auteur avant la review

1. Dans gitlab :
    - choisir un ticket dans la colonne "open"
    - se l'affecter
    - le passer dans la colonne "dev"
    - sur la page de détail du ticket :
        - lire la description du ticket
        - créer la merge request
2. Sur votre machine :
    - récupérer la dernière version du projet (`git pull`)
    - se placer sur la branche de la merge request (`git switch <branch>`)
    - coder !
    - vérifier que tout va bien
    - envoyer les modifications à gitlab : `git add . && git commit -m "<votre message>" && git push`
3. Passer en review dans gitlab :
    - passer le ticket dans la colonne "review"

## Reviewer

1. début de la review
    - choisir un ticket dans la colonne "review"
    - aller sur la merge request associée
    - se positionner en tant que reviewer
    - aller sur l'onglet "changes"
    - ajouter des commentaires sur les anomalies, discuter avec l'auteur
    - attendre les retours / corrections de l'auteur
2. après retour de l'auteur (nouveau commit ou réponse aux commentaires)
    - marquer comme résolue les threads qui le sont
    - analyser à nouveau le code
    - si il y a encore des anomalies => ajouter des commentaires et attendre les retours (retour à l'étape 2)
3. Après correction de toute les anomalies
    - marquer son approval
    - éventuellement, valider la merge request

## Auteur après review

- prendre connaissances des commentaires 
- discuter avec le(s) reviewers
- appliquer les changements
- attendre les nouveaux commentaires et recommencer 